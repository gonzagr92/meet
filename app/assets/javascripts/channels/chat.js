$(document).ready(function (){
  $('.contact-main.channel').each(function(i, channel){
    var room = $(channel).attr('data-channel');

    App.chat = App.cable.subscriptions.create({
        channel: "ChatChannel",
        room: room
      }, {
        connected: function() {
          console.log('Conected');
        },
        disconnected: function() {

        },
        received: function(data) {
          console.log(data);
          var message = data['message_info'];
          var $chatter_box = $('#chatter-box');
          var user_id = $("#member_info").attr('data-member');

          if( message.channel_id == $chatter_box.attr('data-channel')) {

            var $html = $('<div />',{html: data['message_html']});
            $html.find('.msg').addClass(message.member.user_id == user_id ? 'me' : 'you');
            $('#chatter-box').append($html.html());
            $('.chatter-box').scrollTop($('.chatter-box')[0].scrollHeight);

          } else {

            var count = $('.contact-main.channel[data-channel="' + message.channel_id + '"]').find('.count');
            if (count && count.length > 0) {
              var num = parseInt($(count).text());
              if (num < 9) {
                num++;
                $(count).html(num.toString());
              }

            } else {
              $('.contact-main.channel[data-channel="' + message.channel_id + '"] .date-container').append('<div class="count">1</div>');
            }
          }

          var writter = (message.member.user_id == user_id) ? 'me' : message.member.first_name || message.member.username;
          $('.contact-main.channel[data-channel="' + message.channel_id + '"] .last-msg').html( writter + ": " + message.body);

          var date = new Date(Date.parse(message.created_at));
          var min = date.getMinutes() < 10 ?  "0" + date.getMinutes() : date.getMinutes();
          var hs = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();

          $('.contact-main.channel[data-channel="' + message.channel_id + '"] .date').html(hs + ':' + min);
        }
    });
  });
})
