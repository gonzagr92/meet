class Member < ApplicationRecord
  include ActiveModel::Serializers::JSON

  belongs_to :user
  belongs_to :event
  has_and_belongs_to_many :channels

  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :username
  attr_accessor :email
  attr_accessor :avatar

  enum role: [:member, :admin, :owner]

  def attributes
    {
      'first_name' => nil,
      'last_name' => nil,
      'username' => nil,
      'user_id' => nil,
      'avatar' => nil
    }
  end

  private
  after_find do |member|
    self.first_name = member.user.first_name;
    self.last_name = member.user.last_name;
    self.username = member.user.username;
    self.email = member.user.email;
    self.avatar = member.user.avatar;
  end
end
