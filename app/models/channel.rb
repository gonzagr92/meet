class Channel < ApplicationRecord
  belongs_to :event
  has_and_belongs_to_many :members
  has_many :messages

end
