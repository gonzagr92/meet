class Event < ApplicationRecord
  require "base64"

  before_create :generate_uid
  belongs_to :owner, class_name: 'User'
  has_many :members
  has_many :channels

  has_attached_file :image, styles: { medium: "300x300>" }, default_url: ActionController::Base.helpers.asset_path('event-default.jpg')
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  validates :name, presence: true
  validates :start_at, presence: true

  attr_accessor :date
  attr_accessor :time

  private
    after_find do |event|
      self.date = event.created_at.strftime('%b %d, %Y');
      self.time = event.created_at.strftime('%I:%M %p');
    end

    def generate_uid
      date = Time.now.to_i
      self.uid = Base64.encode64(date.to_s)[0..-4]
    end
end
