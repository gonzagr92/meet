class Message < ApplicationRecord
  belongs_to :member
  belongs_to :channel

  attr_accessor :time

  after_create_commit {
    MessageBroadcastJob.perform_later(self)
  }

  private

  after_find do |message|
    self.time = message.created_at.strftime("%H:%M")
  end
end
