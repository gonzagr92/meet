class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_attached_file :avatar, styles: { medium: "300x300>" }, default_url: ActionController::Base.helpers.asset_path('user-default.png')
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  def username
   return email.split('@')[0].capitalize
  end
end
