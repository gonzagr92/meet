class EventsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  # GET /events
  # GET /events.json
  def index
    memberships = Member.where(user_id: current_user.id)

    @events = []
    memberships.each do |member|
      @events.push(member.event)
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    if home_params.blank?
      @event = Event.new
    else
      @event = Event.new(home_params)
    end
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)

    @event.owner = current_user
    @event.members << Member.new(user: current_user, role: :owner)
    @event.channels << Channel.new(name: 'General', members: @event.members)

    respond_to do |format|
      if @event.save
        format.html { redirect_to events_path, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to events_path, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.members.destroy_all
    @event.channels.destroy_all
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      sanitaize_params = params.require(:event).permit(:date, :time, :start_at, :name, :description, :image)
      sanitaize_params[:start_at] = DateTime.parse(sanitaize_params[:date] + ' ' + sanitaize_params[:time]) unless sanitaize_params[:date].blank? && sanitaize_params[:time].blank?
      return sanitaize_params
    end

    def home_params
      unless params[:event].blank?
        params.require(:event).permit(:date, :time)
      end
    end
end
