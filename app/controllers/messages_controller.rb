class MessagesController < ApplicationController
  before_action :authenticate_user!

  def create
    @message = Message.new(message_params)
    @member =  Member.find_by(id: @message.member_id,user: current_user)
    @message.save
  end

  private
    def message_params
      params.require(:message).permit(:body, :member_id, :channel_id)
    end
end
