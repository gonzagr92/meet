class RoomController < ApplicationController
  before_action :authenticate_user!

  def index
    @event = Event.find_by(uid: params[:uid])

    #TODO: If not event redirect 404
    @member = Member.find_by(event: @event, user: current_user)
    if @member.blank?
      @member = @event.members.create(user: current_user, role: :member)
      @member.channels << @event.channels.where(name: "General")
    end

    @message = Message.new
  end

  def channel
    @event = Event.find_by(uid: params[:uid])

    @member = Member.find_by(user: current_user, event: @event)
    unless @member.blank?
      @channel = @member.channels.find(params[:id])
    end

    @message = Message.new(member: @member, channel: @channel)
  end

  def create_channel
    event = Event.find_by(uid: params[:uid])

    host_member = Member.find_by(event: event, user: current_user)
    guest_member = Member.find_by(event: event, id: params[:id])

    return redirect_to room_url(event.uid), alert: 'Invalid member id.' if host_member.blank? || guest_member.blank?
    return redirect_to room_url(event.uid), alert: 'Cannot create a channel with yourself.' if host_member == guest_member

    channels = host_member.channels.joins(:members).where(members: {id: guest_member.id})
    unless channels.blank? || channels.count < 1
      channels.each do |channel|
        return redirect_to room_url(event.uid), alert: 'Channel is alredy created.' if channel.members.count == 2
      end
    end

    channel = Channel.create(event: event)
    channel.members << Member.find_by(event: event, user: current_user)
    channel.members << Member.find_by(event: event, id: params[:id])
    redirect_to room_url(event.uid), notice: 'Channel was successfully created.'

  end
end
