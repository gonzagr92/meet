class MembersController < ApplicationController
  def show
    @members = Member.where(event: params[:event_id])
    @event = Event.find(params[:event_id])
  end
end
