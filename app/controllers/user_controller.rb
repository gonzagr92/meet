class UserController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:edit, :update]

  def edit
  end

  def update
    respond_to do |format|
      #@user.build_job
      #@user.build_education
      if @user.update(user_params)
        format.html { redirect_to user_path, notice: 'User was successfully updated.' }
        format.json { render :edit, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :linkedin_url, :who_am_i,
        :company_name, :position,
        :degree, :level, :institution)
    end

    def set_user
      @user = User.find(current_user.id)
    end
end
