class MessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message)
    ActionCable.server.broadcast "chat_#{message.channel.id}", {
      message_html: render_message(message),
      message_info: message.as_json(:include => :member)
    }
  end

  private
  def render_message(message)
    MessagesController.render(
      partial: 'message',
      locals: {
        "@message": message
      }
    )
  end
end
