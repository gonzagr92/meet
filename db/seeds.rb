# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


owner = User.create(email: 'gonza.gr92@gmail.com', password: '12qweasd', password_confirmation: '12qweasd')
user = User.create(email: 'gonza@gmail.com', password: '12qweasd', password_confirmation: '12qweasd')
admin = User.create(email: 'gonza@meet.com', password: '12qweasd', password_confirmation: '12qweasd')

evt = Event.create(
  name: 'MeetUp Docker',
  description: 'Antes de que termine el año tenemos que encontrarnos y nerdear con Docker, escuchamos un par de presentaciones, hacemos laboratorios y networking con los colegas.',
  start_at: DateTime.now,
  owner: owner
)

m_owner = Member.create(user: owner, event: evt, role: :owner)
m_member = Member.create(user: user, event: evt, role: :member)
m_admin = Member.create(user: admin, event: evt, role: :admin)

general = Channel.create(event: evt, name: 'General')

general.members << m_owner
general.members << m_member
general.members << m_admin

general.messages << Message.new(member: m_member, body: 'Hola!')
