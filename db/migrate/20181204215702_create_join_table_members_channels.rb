class CreateJoinTableMembersChannels < ActiveRecord::Migration[5.1]
  def change
    create_join_table :members, :channels do |t|
      # t.index [:member_id, :channel_id]
      # t.index [:channel_id, :member_id]
    end
  end
end
