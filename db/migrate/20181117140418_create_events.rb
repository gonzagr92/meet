class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.datetime :start_at
      t.string :name
      t.text :description
      t.string :uid, unique: true, index: true

      t.references :owner, index: true, foreign_key: {to_table: :users}
      #t.references :administrators, index: true, foreign_key: {to_table: :users}
      
      t.timestamps
    end

    add_attachment :events, :image
  end
end
