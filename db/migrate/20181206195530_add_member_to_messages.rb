class AddMemberToMessages < ActiveRecord::Migration[5.1]
  def change
    add_reference :messages, :member, foreign_key: true
  end
end
