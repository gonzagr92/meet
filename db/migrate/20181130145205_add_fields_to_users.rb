class AddFieldsToUsers < ActiveRecord::Migration[5.1]
  def change
    #Personal Information
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :linkedin_url, :string
    add_column :users, :who_am_i, :string

    #Employment
    add_column :users, :position, :string
    add_column :users, :company_name, :string

    #Education
    add_column :users, :degree, :string
    add_column :users, :level, :string
    add_column :users, :institution, :string

    add_attachment :users, :avatar
  end
end
