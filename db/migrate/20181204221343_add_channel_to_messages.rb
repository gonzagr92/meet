class AddChannelToMessages < ActiveRecord::Migration[5.1]
  def change
    add_reference :messages, :channel, foreign_key: true
  end
end
