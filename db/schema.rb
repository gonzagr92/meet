# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181206195530) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "channels", force: :cascade do |t|
    t.string "name"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_channels_on_event_id"
  end

  create_table "channels_members", id: false, force: :cascade do |t|
    t.bigint "member_id", null: false
    t.bigint "channel_id", null: false
  end

  create_table "events", force: :cascade do |t|
    t.datetime "start_at"
    t.string "name"
    t.text "description"
    t.string "uid"
    t.bigint "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.bigint "image_file_size"
    t.datetime "image_updated_at"
    t.index ["owner_id"], name: "index_events_on_owner_id"
    t.index ["uid"], name: "index_events_on_uid"
  end

  create_table "members", force: :cascade do |t|
    t.integer "role", default: 0
    t.bigint "user_id"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_members_on_event_id"
    t.index ["user_id"], name: "index_members_on_user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "channel_id"
    t.bigint "member_id"
    t.index ["channel_id"], name: "index_messages_on_channel_id"
    t.index ["member_id"], name: "index_messages_on_member_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "linkedin_url"
    t.string "who_am_i"
    t.string "position"
    t.string "company_name"
    t.string "degree"
    t.string "level"
    t.string "institution"
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.bigint "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "channels", "events"
  add_foreign_key "events", "users", column: "owner_id"
  add_foreign_key "members", "events"
  add_foreign_key "members", "users"
  add_foreign_key "messages", "channels"
  add_foreign_key "messages", "members"
end
