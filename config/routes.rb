Rails.application.routes.draw do
  devise_for :users
  resources :events

  get   'user/edit',           to: 'user#edit',             as: :user
  put   'user/edit',           to: 'user#update'

  get   'room/:uid',           to: 'room#index',            as: :room
  get   'channel/:uid/:id',    to: 'room#channel',          as: :channel
  post  'channel/:uid/:id',    to: 'room#create_channel',   as: :create_channel
  post  'message',             to: 'messages#create',       as: :messages
  post  'members/:event_id',   to: 'members#show',          as: :members
  post  'events/new',          to: 'events#new',            as: :home_new

  #OmniAuth Routes
  get '/auth/:provider/callback', to: 'oauth#callback', as: 'oauth_callback'
  get '/auth/failure', to: 'oauth#failure', as: 'oauth_failure'

  root to: 'index#home'
end
